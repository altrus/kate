# Irish translation of katebuild-plugin
# Copyright (C) 2009 This_file_is_part_of_KDE
# This file is distributed under the same license as the katebuild-plugin package.
# Kevin Scannell <kscanne@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kdesdk/katebuild-plugin.po\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-17 01:37+0000\n"
"PO-Revision-Date: 2009-01-20 10:11-0500\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? "
"3 : 4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kevin Scannell"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kscanne@gmail.com"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Aschur"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, fuzzy, kde-format
#| msgid "Build Plugin"
msgid "Build again"
msgstr "Breiseán Tógála"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr ""

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr ""

#: buildconfig.cpp:27
#, kde-format
msgid "Automatically switch to output pane on executing the selected target"
msgstr ""

#: buildconfig.cpp:40
#, fuzzy, kde-format
#| msgid "Build Plugin"
msgid "Build & Run"
msgstr "Breiseán Tógála"

#: buildconfig.cpp:46
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build & Run Settings"
msgstr "Theip ar thógáil."

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1225
#, kde-format
msgid "Build"
msgstr "Tóg"

#: plugin_katebuild.cpp:222
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Select Target..."
msgstr "Theip ar thógáil."

#: plugin_katebuild.cpp:227
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build Selected Target"
msgstr "Theip ar thógáil."

#: plugin_katebuild.cpp:232
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build and Run Selected Target"
msgstr "Theip ar thógáil."

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr "Stad"

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr ""

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr ""

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Socruithe Sprice"

#: plugin_katebuild.cpp:403
#, fuzzy, kde-format
#| msgid "Build command:"
msgid "Build Information"
msgstr "Ordú tógála:"

#: plugin_katebuild.cpp:620
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "Níor sonraíodh comhad nó comhadlann ar bith le tógáil ann."

#: plugin_katebuild.cpp:624
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr "Ní comhad logánta é \"%1\". Ní féidir comhaid neamhlogánta a thiomsú."

#: plugin_katebuild.cpp:674
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""

#: plugin_katebuild.cpp:688
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "Níorbh fhéidir \"%1\" a rith. Stádas scortha = %2"

#: plugin_katebuild.cpp:703
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr ""

#: plugin_katebuild.cpp:810
#, kde-format
msgid "No target available for building."
msgstr ""

#: plugin_katebuild.cpp:824
#, fuzzy, kde-format
#| msgid "There is no file or directory specified for building."
msgid "There is no local file or directory specified for building."
msgstr "Níor sonraíodh comhad nó comhadlann ar bith le tógáil ann."

#: plugin_katebuild.cpp:830
#, kde-format
msgid "Already building..."
msgstr ""

#: plugin_katebuild.cpp:857
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Building target <b>%1</b> ..."
msgstr "Theip ar thógáil."

#: plugin_katebuild.cpp:871
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr ""

#: plugin_katebuild.cpp:907
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:913
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Aimsíodh earráid amháin."
msgstr[1] "Aimsíodh %1 earráid."
msgstr[2] "Aimsíodh %1 earráid."
msgstr[3] "Aimsíodh %1 n-earráid."
msgstr[4] "Aimsíodh %1 earráid."

#: plugin_katebuild.cpp:917
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Aimsíodh rabhadh amháin."
msgstr[1] "Aimsíodh %1 rabhadh."
msgstr[2] "Aimsíodh %1 rabhadh."
msgstr[3] "Aimsíodh %1 rabhadh."
msgstr[4] "Aimsíodh %1 rabhadh."

#: plugin_katebuild.cpp:920
#, fuzzy, kde-format
#| msgid "Found one error."
#| msgid_plural "Found %1 errors."
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "Aimsíodh earráid amháin."
msgstr[1] "Aimsíodh %1 earráid."
msgstr[2] "Aimsíodh %1 earráid."
msgstr[3] "Aimsíodh %1 n-earráid."
msgstr[4] "Aimsíodh %1 earráid."

#: plugin_katebuild.cpp:925
#, kde-format
msgid "Build failed."
msgstr "Theip ar thógáil."

#: plugin_katebuild.cpp:927
#, kde-format
msgid "Build completed without problems."
msgstr "D'éirigh leis an tógáil gan fadhb ar bith."

#: plugin_katebuild.cpp:932
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:956
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr ""

#: plugin_katebuild.cpp:1182
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark an error."
#| msgid "error"
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "earráid"

#: plugin_katebuild.cpp:1185
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark a warning."
#| msgid "warning"
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "rabhadh"

#: plugin_katebuild.cpp:1188
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr ""

#: plugin_katebuild.cpp:1191
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "tagairt gan sainmhíniú"

#: plugin_katebuild.cpp:1224 TargetModel.cpp:285 TargetModel.cpp:297
#, fuzzy, kde-format
#| msgid "Targets"
msgid "Target Set"
msgstr "Spriocanna"

#: plugin_katebuild.cpp:1226
#, kde-format
msgid "Clean"
msgstr "Glan"

#: plugin_katebuild.cpp:1227
#, kde-format
msgid "Config"
msgstr "Cumraíocht"

#: plugin_katebuild.cpp:1228
#, fuzzy, kde-format
#| msgid "Config"
msgid "ConfigClean"
msgstr "Cumraíocht"

#: plugin_katebuild.cpp:1419
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr ""

#: TargetHtmlDelegate.cpp:50
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:52
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:101
#, fuzzy, kde-format
#| msgid "Leave empty to use the directory of the current document. "
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr "Fág folamh é chun comhadlann na cáipéise reatha a úsáid. "

#: TargetHtmlDelegate.cpp:105
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Úsáid:\n"
"\"%f\" don chomhad reatha\n"
"\"%d\" do chomhadlann an chomhaid reatha\"%n\" don chomhad reatha gan iarmhír"

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr ""

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr ""

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr ""

#: TargetModel.cpp:627
#, fuzzy, kde-format
#| msgid "Working directory"
msgid "Working Directory / Command"
msgstr "Comhadlann oibre"

#: TargetModel.cpp:630
#, fuzzy, kde-format
#| msgid "Clean command:"
msgid "Run Command"
msgstr "Ordú glanta:"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr ""

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr ""

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr ""

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr ""

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr ""

#: targets.cpp:48
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build and run selected target"
msgstr "Theip ar thógáil."

#: targets.cpp:52
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Move selected target up"
msgstr "Theip ar thógáil."

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr ""

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&Tóg"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr ""

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr ""

#, fuzzy
#~| msgid "Build"
#~ msgid "build"
#~ msgstr "Tóg"

#, fuzzy
#~| msgid "Clean"
#~ msgid "clean"
#~ msgstr "Glan"

#, fuzzy
#~| msgid "Build failed."
#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "Theip ar thógáil."

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "Comhad"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "Líne"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "Teachtaireacht"

#~ msgid "Next Error"
#~ msgstr "An Chéad Earráid Eile"

#~ msgid "Previous Error"
#~ msgstr "An Earráid Roimhe Seo"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark an error."
#~| msgid "error"
#~ msgid "Error"
#~ msgstr "earráid"

#, fuzzy
#~| msgid "Warnings"
#~ msgid "Warning"
#~ msgstr "Rabhaidh"

#, fuzzy
#~| msgid "Errors"
#~ msgid "Only Errors"
#~ msgstr "Earráidí"

#, fuzzy
#~| msgid "Errors && Warnings"
#~ msgid "Errors and Warnings"
#~ msgstr "Earráidí agus Rabhaidh"

#, fuzzy
#~| msgid "Build Output"
#~ msgid "Parsed Output"
#~ msgstr "Aschur Tógála"

#, fuzzy
#~| msgid "Build Output"
#~ msgid "Full Output"
#~ msgstr "Aschur Tógála"

#, fuzzy
#~| msgid "Build failed."
#~ msgid "Build and Run Default Target"
#~ msgstr "Theip ar thógáil."

#, fuzzy
#~| msgid "Build failed."
#~ msgid "Build Previous Target"
#~ msgstr "Theip ar thógáil."

#, fuzzy
#~| msgid "Config"
#~ msgid "config"
#~ msgstr "Cumraíocht"

#, fuzzy
#~| msgid "Build Plugin"
#~ msgid "Kate Build Plugin"
#~ msgstr "Breiseán Tógála"

#~ msgid "Build Output"
#~ msgstr "Aschur Tógála"

#, fuzzy
#~| msgid "Next Target"
#~ msgid "Next Set of Targets"
#~ msgstr "An Chéad Sprioc Eile"

#, fuzzy
#~| msgid "Target %1"
#~ msgid "Target Set %1"
#~ msgstr "Sprioc %1"

#, fuzzy
#~| msgid "Targets"
#~ msgid "Target"
#~ msgstr "Spriocanna"

#, fuzzy
#~| msgid "Targets"
#~ msgid "Target:"
#~ msgstr "Spriocanna"

#, fuzzy
#~| msgid "Next Target"
#~ msgid "Sets of Targets"
#~ msgstr "An Chéad Sprioc Eile"

#~ msgid "Make Results"
#~ msgstr "Torthaí 'make'"

#~ msgid "Others"
#~ msgstr "Eile"

#~ msgid "Quick Compile"
#~ msgstr "Tiomsú Tapa"

#~ msgid "The custom command is empty."
#~ msgstr "Tá an t-ordú saincheaptha folamh."

#~ msgid "New"
#~ msgstr "Nua"

#~ msgid "Copy"
#~ msgstr "Cóipeáil"

#~ msgid "Delete"
#~ msgstr "Scrios"

#~ msgid "Quick compile"
#~ msgstr "Tiomsú tapa"

#~ msgid "Run make"
#~ msgstr "Rith make"

#~ msgid "Break"
#~ msgstr "Briseadh"
